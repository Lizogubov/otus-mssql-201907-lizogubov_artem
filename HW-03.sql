-- 1. Выберите сотрудников, которые являются продажниками, и еще не сделали ни одной продажи.

SELECT
	p.PersonID, p.FullName
FROM Application.People p
WHERE p.PersonID NOT IN(SELECT SalespersonPersonID FROM Sales.Invoices)
AND p.IsSalesperson = 1

-- 2. Выберите товары с минимальной ценой (подзапросом), 2 варианта подзапроса.

SELECT * 
FROM Warehouse.StockItems si
WHERE si.UnitPrice <= ALL(SELECT UnitPrice FROM Warehouse.StockItems)
	
SELECT * FROM Warehouse.StockItems si
	WHERE si.UnitPrice = (SELECT MIN(UnitPrice) FROM Warehouse.StockItems)

-- 3. Выберите информацию по клиентам, которые перевели компании 5 максимальных платежей из [Sales].[CustomerTransactions] 
-- представьте 3 способа (в том числе с CTE)

SELECT  c.*
FROM Sales.Customers AS c
WHERE c.CustomerID IN (
	SELECT 
		TOP 5 CustomerID
	FROM [Sales].[CustomerTransactions] 
	GROUP BY CustomerID 
	ORDER BY MAX(TransactionAmount) DESC
)

SELECT  c.*
FROM Sales.Customers AS c
JOIN (
	SELECT 
		TOP 5 CustomerID
	FROM [Sales].[CustomerTransactions] 
	GROUP BY CustomerID 
	ORDER BY MAX(TransactionAmount) DESC
) AS oc ON c.CustomerID = oc.CustomerID

;WITH CustomersOrderedByMaxTransactionsCTE(CustomerID) AS
( 
	SELECT 
		TOP 5 CustomerID
	FROM [Sales].[CustomerTransactions] 
	GROUP BY CustomerID 
	ORDER BY MAX(TransactionAmount) DESC
)
SELECT  c.*
FROM Sales.Customers AS c
WHERE c.CustomerID IN (SELECT CustomerID FROM CustomersOrderedByMaxTransactionsCTE)

-- 4. Выберите города (ид и название), в которые были доставлены товары, входящие в тройку самых дорогих товаров, 
-- а также Имя сотрудника, который осуществлял упаковку заказов

;WITH MostExpensiveStockItems(StockItemID) AS (
	SELECT TOP 
		3 StockItemID 
	FROM Warehouse.StockItems 
	ORDER BY UnitPrice DESC
)
SELECT DISTINCT c.CityID, c.CityName, p1.FullName
FROM MostExpensiveStockItems
JOIN [Warehouse].[StockItemTransactions] AS sit ON sit.StockItemID = MostExpensiveStockItems.StockItemID
JOIN [Sales].[Customers] cus ON cus.CustomerID = sit.CustomerID
JOIN Application.Cities c ON c.CityID = cus.DeliveryCityID
JOIN Sales.Invoices i ON cus.CustomerID = i.CustomerID
JOIN Application.People p1 ON p1.PersonID = i.PackedByPersonID


SELECT DISTINCT c.CityID, c.CityName, p1.FullName
FROM ( 
	SELECT TOP 3 StockItemID 
	FROM Warehouse.StockItems  
	ORDER BY UnitPrice DESC
) as MostExpensiveStockItems
JOIN [Warehouse].[StockItemTransactions] AS sit ON sit.StockItemID = MostExpensiveStockItems.StockItemID
JOIN [Sales].[Customers] cus ON cus.CustomerID = sit.CustomerID
JOIN Application.Cities c ON c.CityID = cus.DeliveryCityID
JOIN Sales.Invoices i ON cus.CustomerID = i.CustomerID
JOIN Application.People p1 ON p1.PersonID = i.PackedByPersonID

-- 5. Объясните, что делает и оптимизируйте запрос:
-- Запрос выбирает Id счета, дату выставления счёта, полное имя менеджера по продажам,
-- суммарную стоимость товаров по линиям счета
-- суммарную стоимость товаров по собранным в заказе товарам для заказов, где есть дата выполнения сбора заказа
-- учитывая только те счета, где сумма по линиям счета больше 27000
-- с сортировкой по общей сумме линии счета


;WITH SalesTotals(InvoiceId, TotalSum) AS ( 
	SELECT
		InvoiceId, SUM(Quantity * UnitPrice) AS TotalSum
	FROM Sales.InvoiceLines
	GROUP BY InvoiceId
	HAVING SUM(Quantity * UnitPrice) > 27000
)
SELECT
	p.FullName AS SalesPersonName,
	i.InvoiceID,
	i.InvoiceDate,
	SalesTotals.TotalSum AS TotalSumByInvoice,
	SUM(ol.PickedQuantity * ol.UnitPrice) AS TotalSumForPickedItems
FROM Sales.Invoices AS i
JOIN SalesTotals ON i.InvoiceID = SalesTotals.InvoiceID
JOIN Application.People AS p ON p.PersonID = i.SalespersonPersonID
JOIN Sales.Orders AS o ON o.OrderID = i.OrderID
JOIN Sales.OrderLines AS ol ON ol.OrderID = o.OrderID
WHERE o.PickingCompletedWhen IS NOT NULL
	AND o.OrderId = i.OrderId
GROUP BY p.FullName, i.InvoiceID, i.InvoiceDate, SalesTotals.TotalSum
ORDER BY TotalSum DESC