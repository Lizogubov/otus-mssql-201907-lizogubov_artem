--Домашнее задание
--Запросы SELECT
--Напишите выборки для того, чтобы получить:

-- 1. Все товары, в которых в название есть пометка urgent или название начинается с Animal

SELECT * 
FROM Warehouse.StockItems
WHERE 
	StockItemName LIKE '%urgent%' 
	OR StockItemName LIKE 'Animal%'

-- 2. Поставщиков, у которых не было сделано ни одного заказа (потом покажем как это делать через подзапрос, сейчас сделайте через JOIN)

SELECT 
	s.*
FROM Purchasing.Suppliers AS s
LEFT JOIN Purchasing.PurchaseOrders AS o ON s.SupplierID = o.SupplierID
WHERE o.SupplierID IS NULL

-- 3. Продажи с названием месяца, в котором была продажа, номером квартала, к которому относится продажа, включите также к какой трети года относится дата - каждая -- треть по 4 месяца, дата забора заказа должна быть задана, с ценой товара более 100$ либо количество единиц товара более 20. 

SELECT 
	DISTINCT i.InvoiceID,
	DATENAME(MONTH, i.InvoiceDate) AS InvoiceMonthName,
	DATEPART(QUARTER, i.InvoiceDate) AS InvoiceQuarterNumber,
	CEILING(MONTH(i.InvoiceDate) / 4.0) AS InvoiceThirdNumber -- ИЛИ MONTH(i.InvoiceDate) - 1)/4 +1
FROM Sales.Invoices AS i
JOIN Sales.InvoiceLines AS il ON i.InvoiceID = il.InvoiceID
WHERE 
	i.ConfirmedDeliveryTime IS NOT NULL
	AND (il.UnitPrice > 100 OR il.Quantity > 20)

-- Добавьте вариант этого запроса с постраничной выборкой пропустив первую 1000 и отобразив следующие 100 записей.
-- Соритровка должна быть по номеру квартала, трети года, дате продажи.

SELECT 
	DISTINCT i.InvoiceID,
	DATENAME(MONTH, i.InvoiceDate) AS InvoiceMonthName,
	DATEPART(QUARTER, i.InvoiceDate) AS InvoiceQuarterNumber,
	CASE 
		WHEN MONTH(i.InvoiceDate) BETWEEN 1 AND 4 THEN 1
		WHEN MONTH(i.InvoiceDate) BETWEEN 5 AND 8 THEN 2
		WHEN MONTH(i.InvoiceDate) BETWEEN 9 AND 12 THEN 3
		ELSE NULL
	END InvoiceThirdNumber,
	i.InvoiceDate
FROM Sales.Invoices AS i
JOIN Sales.InvoiceLines AS il ON i.InvoiceID = il.InvoiceID
WHERE 
	i.ConfirmedDeliveryTime IS NOT NULL
	AND (il.UnitPrice > 100 OR il.Quantity > 20)
ORDER BY InvoiceQuarterNumber, InvoiceThirdNumber, i.InvoiceDate
OFFSET 1000 ROWS FETCH NEXT 100 ROWS ONLY; 


-- 4. Заказы поставщикам, которые были исполнены за 2014й год с доставкой Road Freight или Post, добавьте название поставщика, имя контактного лица принимавшего заказ

SELECT 
	po.*,
	s.SupplierName,
	p.FullName AS ContactPersonName
FROM Purchasing.PurchaseOrders AS po
JOIN Application.DeliveryMethods AS dm ON dm.DeliveryMethodID = po.DeliveryMethodID
JOIN Purchasing.Suppliers AS s ON s.SupplierID = po.SupplierID
JOIN Application.People AS p ON p.PersonID = po.ContactPersonID
WHERE 
	YEAR(po.OrderDate) = 2014
	AND (dm.DeliveryMethodName = 'Road Freight' OR dm.DeliveryMethodName = 'Post')
	AND po.IsOrderFinalized = 1
	
-- 5. 10 последних по дате продаж с именем клиента и именем сотрудника, который оформил заказ.

SELECT top 10
	i.*,
	c.CustomerName AS OrderCustomerName,
	p.FullName AS OrderSalespersonName
FROM Sales.Invoices AS i
JOIN Sales.Orders AS o ON o.OrderID = i.OrderID
JOIN Sales.Customers AS c ON c.CustomerID = o.CustomerID
JOIN Application.People AS p ON p.PersonID = i.SalespersonPersonID
ORDER BY InvoiceDate DESC

-- 6. Все ид и имена клиентов и их контактные телефоны, которые покупали товар Chocolate frogs 250g

SELECT 
	c.CustomerID,
	c.CustomerName,
	c.PhoneNumber
FROM Sales.Customers AS c
JOIN warehouse.StockItemTransactions AS sit ON sit.CustomerID = c.CustomerID
JOIN warehouse.StockItems AS si ON si.StockItemID = sit.StockItemID
WHERE si.StockItemName = 'Chocolate frogs 250g'